import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;
import java.util.Set;
/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
        time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
        time1.addJogador("Atacante", new Jogador(10, "Beltrano"));
        
        Time time2 = new Time();
        time2.addJogador("Goleiro", new Jogador(1, "João"));
        time2.addJogador("Lateral", new Jogador(7, "José"));
        time2.addJogador("Atacante", new Jogador(15, "Mário"));
        
        Set<String> keys = time1.getJogadores().keySet();
        /*Posição    Time 1        Time 2
        Goleiro    1 - Fulano    1 - João
        Lateral    4 - Ciclano   7 - José
        Atacante   10 - Beltrano 15 - Mário*/
        System.out.println("Posição\tTime 1\t Time 2");
        for(String chave:keys){
            System.out.println(chave+
                "\t"+time1.getJogadores().get(chave).toString()+
                "\t"+time2.getJogadores().get(chave).toString());            
        }
        /*keys.forEach((chave) -> {
            System.out.println(chave+"\t"+time1.getJogadores().get(chave).toString()+"\t"+time2.getJogadores().get(chave).toString());
        });*/
    }
}