package utfpr.ct.dainf.if62c.pratica;
import java.util.HashMap;
/**
 * @author Marcelo Guimarães da Costa <marcelo@unicode.com.br> 
 */
public class Time {
    private HashMap<String,Jogador> jogadores;
    
    public Time(){
        jogadores = new HashMap<>();
    }
    
    public HashMap<String,Jogador> getJogadores(){
        return jogadores;
    }
    
    public void addJogador(String key, Jogador jogador){
        jogadores.put(key, jogador);
    }
}
